const { Router } = require('express');
const router = Router();

const { getEmployees, getEmployeeById, createEmployee, updateEmployee, deleteEmployee } = require('../controllers/controller');

router.get('/employees', getEmployees);
router.get('/employees/:id', getEmployeeById);
router.post('/employees', createEmployee);
router.put('/employees/:id', updateEmployee)
router.delete('/employees/:id', deleteEmployee);

module.exports = router;